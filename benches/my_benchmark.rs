use criterion::{criterion_group, criterion_main, Criterion};
use std::collections::HashMap;

use dynfmt::{Format, SimpleCurlyFormat};
use handlebars::Handlebars;


fn handlebars_test() {
    let reg = Handlebars::new();
    let foo = HashMap::from([
        ("a", "A"),
        ("b", "B"),
        ("c", "C"),
        ("d", "D"),
        ("z", "Z"),
    ]);
    let mut formatted = String::from("");
    for _ in 0..100 {
        formatted = reg.render_template("a {{a}} b {{b}} c {{c}} d {{d}}", &foo).unwrap();
    };
    assert_eq!("a A b B c C d D", formatted);
}

fn dynfmt_test() {
    let foo = HashMap::from([
        ("a", "A"),
        ("b", "B"),
        ("c", "C"),
        ("d", "D"),
        ("z", "Z"),
    ]);
    let mut formatted = String::from("");
    for _ in 0..100 {
        formatted = SimpleCurlyFormat.format("a {a} b {b} c {c} d {d}", &foo).unwrap().to_string();
    }
    assert_eq!("a A b B c C d D", formatted);
}

fn string_replace_test() {
    let mut formatted = String::from("");
    for _ in 0..100 {
        formatted = String::from("a %a b %b c %c d %d")
            .replace("%a", "A")
            .replace("%b", "B")
            .replace("%c", "C")
            .replace("%d", "D")
            .replace("%z", "Z");
    }
    assert_eq!("a A b B c C d D", formatted);
}

fn string_replace_with_map_test() {
    let foo = HashMap::from([
        ("a", "A"),
        ("b", "B"),
        ("c", "C"),
        ("d", "D"),
        ("z", "Z"),
    ]);
    let mut formatted = String::from("");
    for _ in 0..100 {
        formatted = String::from("a %a b %b c %c d %d")
            .replace("%a", &foo.get("a").unwrap())
            .replace("%b", &foo.get("b").unwrap())
            .replace("%c", &foo.get("c").unwrap())
            .replace("%d", &foo.get("d").unwrap())
            .replace("%z", &foo.get("z").unwrap());
    }
    assert_eq!("a A b B c C d D", formatted);
}

fn criterion_benchmark(c: &mut Criterion) {
    println!("dyn_fmt seems not to support named fields");
    c.bench_function("dynfmt", |b| b.iter(|| dynfmt_test()));
    c.bench_function("handlebars", |b| b.iter(|| handlebars_test()));
    c.bench_function("string_replace", |b| b.iter(|| string_replace_test()));
    c.bench_function("string_replace_with_map", |b| b.iter(|| string_replace_with_map_test()));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
